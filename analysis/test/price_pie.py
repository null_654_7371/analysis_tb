# from pyecharts.charts import Pie
#
# price_cut_num = df.groupby('price_cut')['purchase_num'].sum()
# data_pair = [list(z) for z in zip(price_cut_num.index, price_cut_num.values)]
#
# # 饼图
# pie1 = Pie(init_opts=opts.InitOpts(width='1350px', height='750px'))
# # 内置富文本
# pie1.add(
#     series_name="sales",
#     radius=["35%", "55%"],
#     data_pair=data_pair,
#     label_opts=opts.LabelOpts(
#         position="outside",
#         formatter="{a|{a}}{abg|}\n{hr|}\n {b|{b}: }{c}  {per|{d}%}  ",
#         background_color="#eee",
#         border_color="#aaa",
#         border_width=1,
#         border_radius=4,
#         rich={
#             "a": {"color": "#999", "lineHeight": 22, "align": "center"},
#             "abg": {
#                 "backgroundColor": "#e3e3e3",
#                 "width": "100%",
#                 "align": "right",
#                 "height": 22,
#                 "borderRadius": [4, 4, 0, 0],
#             },
#             "hr": {
#                 "borderColor": "#aaa",
#                 "width": "100%",
#                 "borderWidth": 0.5,
#                 "height": 0,
#             },
#             "b": {"fontSize": 16, "lineHeight": 33},
#             "per": {
#                 "color": "#eee",
#                 "backgroundColor": "#334455",
#                 "padding": [2, 4],
#                 "borderRadius": 2,
#             },
#         },
#     ),
# )
# pie1.set_global_opts(legend_opts=opts.LegendOpts(pos_left="left", pos_top='30%', orient="vertical"),
#                      toolbox_opts=opts.ToolboxOpts(),
#                      title_opts=opts.TitleOpts(title='热干面不同价格销量占比'))
# pie1.set_series_opts(
#     tooltip_opts=opts.TooltipOpts(trigger="item", formatter="{a} <br/>{b}: {c} ({d}%)")
# )
# make_snapshot(snapshot, pie1.render("../html/province_map.html"), "../png/province_map.png")
