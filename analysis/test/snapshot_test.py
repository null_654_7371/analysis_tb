from pyecharts.charts import Bar
from pyecharts.render import make_snapshot
import pyecharts.options as opts
from pyecharts.charts import Line
from pyecharts.faker import Faker
from snapshot_pyppeteer import snapshot
# #
# # # 使用 snapshot-selenium 渲染图片
# # from snapshot_selenium import snapshot
# #
# # bar = (
# #     Bar()
# #     .add_xaxis(["衬衫", "羊毛衫", "雪纺衫", "裤子", "高跟鞋", "袜子"])
# #     .add_yaxis("商家A", [5, 20, 36, 10, 75, 90])
# # )
# # make_snapshot(snapshot, bar.render(), "bar.png")
#
# from pyecharts import options as opts
# from pyecharts.charts import Bar
# from pyecharts.render import make_snapshot
#
# from snapshot_selenium import snapshot
#
# def bar_chart() -> Bar:
#     c = (
#         Bar()
#         .add_xaxis(["衬衫", "毛衣", "领带", "裤子", "风衣", "高跟鞋", "袜子"])
#         .add_yaxis("商家A", [114, 55, 27, 101, 125, 27, 105])
#         .add_yaxis("商家B", [57, 134, 137, 129, 145, 60, 49])
#         .reversal_axis()
#         .set_series_opts(label_opts=opts.LabelOpts(position="right"))
#         .set_global_opts(title_opts=opts.TitleOpts(title="Bar-测试渲染图片"))
#     )
#     return c
#
# make_snapshot(snapshot, bar_chart().render(), "bar0.png")



#
# def bar_base() -> Bar:
#     c = (
#         Bar()
#         .add_xaxis(Faker.choose())
#         .add_yaxis("商家A", Faker.values())
#         .add_yaxis("商家B", Faker.values())
#         .set_global_opts(title_opts=opts.TitleOpts(title="Bar-基本示例", subtitle="我是副标题"))
#     )
#     make_snapshot(snapshot, c.render("../html/snapshot.html"), "../png/bar.png")
def bar_base() -> Line:
    c = (
        Line()
        .add_xaxis(Faker.choose())
        .add_yaxis("商家A", Faker.values())
        .add_yaxis("商家B", Faker.values())
        .set_global_opts(title_opts=opts.TitleOpts(title="Line-基本示例"))
    )
    make_snapshot(snapshot, c.render("../html/line.html"), "../png/line.png")

if __name__ == '__main__':
    bar_base()