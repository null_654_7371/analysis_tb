# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# https://docs.scrapy.org/en/latest/topics/items.html

import scrapy
from analysis.models import Good


class AnalysisItem(scrapy.Item):
    # define the fields for your item here like:
    # name = scrapy.Field()
    url = scrapy.Field()
    price = scrapy.Field()
    name = scrapy.Field()
    num = scrapy.Field()
    shop_name = scrapy.Field()
    location = scrapy.Field()
    province_name = scrapy.Field()
    # pass

    def save(self):
        good = Good()
        # good.id = IntegerField(primary_key=True, verbose_name="商品id")
        good.name = self["name"]
        good.url = self["url"]
        good.num = self.get("num", 0)
        good.shop_name = self["shop_name"]
        good.price = self.get("price", 0)
        good.location = self["location"]
        good.province_name = self["province_name"]

        # existed_topics = Good.select().where(Good.id == topic.id)
        # if existed_topics:
        #     topic.save()
        # else:
        good.save(force_insert=True)
