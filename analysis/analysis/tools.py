import re
import random
import time
from selenium import webdriver
from analysis.settings import *
from selenium.webdriver.common.by import By
from selenium.webdriver import ActionChains
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
# import pickle

def data_cleaning(data):
    if ' ' in data:
        data = re.sub(' ', '', data)
    if "'" in data:
        data = re.sub("'", '', data)
    if r'\n' in data:
        data = re.sub(r'\\n', '', data)
    return data

def register():
    while True:
        chrome_options = Options()
        # 设置headless模式
        chrome_options.add_argument("--headless")
        # 谷歌文档提到需要加上这个属性来规避bug
        chrome_options.add_argument('--disable-gpu')
        # 设置不加载图片
        chrome_options.add_argument("blink-settings=imagesEnabled=false")
        browser = webdriver.Chrome(executable_path="D:/37380/Download/chromedriver/test/chromedriver.exe",
                                   chrome_options=chrome_options)
        # browser = webdriver.Chrome(executable_path="D:/37380/Download/chromedriver_win32/chromedriver.exe",
        #                            chrome_options=chrome_options)
        # browser.get("http://csdn.net/")
        browser.get("https://login.taobao.com/member/login.jhtml")
        try:
            time.sleep(2)
            browser.implicitly_wait(20)
            time.sleep(0.1)
            browser.execute_script("Object.defineProperties(navigator,{webdriver:{get:() => false}})")
        # js1 = "Object.defineProperties(navigator,{ webdriver:{ get: () => false } })"
        # js2 = "window.navigator.chrome = { runtime: {},  }; "
        # js3 = "Object.defineProperty(navigator, 'languages', { get: () => ['en-US', 'en'] }); "
        # js4 = "Object.defineProperty(navigator, 'plugins', { get: () => [1, 2, 3, 4, 5,6], }); "
        # browser.execute_script(js1)
        # browser.execute_script(js2)
        # browser.execute_script(js3)
        # browser.execute_script(js4)


            input = WebDriverWait(browser, 10).until(
                EC.presence_of_element_located((By.CLASS_NAME, 'password-login-tab-item')))
            input.click()
        except Exception as e:
            print(e)

        user = browser.find_element_by_id('fm-login-id')
        user.send_keys(USER)
        time.sleep(random.random() * 2)
        password = browser.find_element_by_id('fm-login-password')
        password.send_keys(PASSWORD)
        time.sleep(random.random() * 1)
        # browser.execute_script("Object.defineProperties(navigator,{webdriver:{get:() => false}})")
        # action = ActionChains(browser)
        # time.sleep(random.random() * 1)
        # butt = browser.find_element_by_id('nc_1_n1z')
        # browser.switch_to.frame(browser.find_element_by_id('_oid_ifr_'))
        # browser.switch_to.default_content()
        # action.click_and_hold(butt).perform()
        # action.move_by_offset(285, 0).perform()
        time.sleep(random.random() * 1)
        button = browser.find_element_by_class_name('password-login')
        time.sleep(random.random() * 2)
        button.click()
        time.sleep(random.random() * 2)
        cookie = browser.get_cookies()
        list = {}
        for cookiez in cookie:
            name = cookiez['name']
            value = cookiez['value']
            list[name] = value
        if len(list) > 10:
            break
        else:
            browser.close()
    return browser,list