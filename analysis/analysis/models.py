from peewee import *

db = MySQLDatabase("spider", host="127.0.0.1", port=3306, user="root", password="123456")


class BaseModel(Model):
    class Meta:
        database = db

#设计数据表的时候有几个重要点一定要注意
"""
char类型， 要设置最大长度
对于无法确定最大长度的字段，可以设置为Text
设计表的时候 采集到的数据要尽量先做格式化处理
default和null=True
 data['url'] = item['url']
        data['name'] = item['name']
        data['price'] = item['price']
        data['num'] = item['num']
        data['shop_name'] = item['shop_name']

        CREATE TABLE `users` (
    `id` int(11) NOT NULL AUTO_INCREMENT,
    `email` varchar(255) COLLATE utf8_bin NOT NULL,
    `password` varchar(255) COLLATE utf8_bin NOT NULL,
    PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin
AUTO_INCREMENT=1 ;
"""


class Good(BaseModel):
    id = IntegerField(primary_key=True, verbose_name="商品id")
    name = CharField(max_length=500, verbose_name="商品名称")
    url = CharField(max_length=500, verbose_name="商品URL")
    num = IntegerField(default=0, verbose_name="数量")
    # comment = IntegerField(default=0, verbose_name="评论数")
    shop_name = CharField(max_length=500, verbose_name="商店名称")
    price = FloatField(default=0.0, verbose_name="商品价格")
    location = CharField(max_length=500, verbose_name="地区")
    province_name = CharField(max_length=500, verbose_name="省份")

if __name__ == "__main__":
    db.create_tables([Good])
