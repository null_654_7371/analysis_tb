from peewee import *
from analysis.models import Good
from pyecharts.charts import Line
from pyecharts.faker import Faker
import pyecharts.options as opts
from pyecharts.render import make_snapshot



#数据的增、删、改、查

if __name__ == "__main__":
    shop_name = []
    num = []
    price = []
    for good in Good.select():
        shop_name.append(good.shop_name)
        num.append(good.num)
        price.append(good.price)

    c = (
        Line()
        .add_xaxis("店铺", shop_name)
        .add_yaxis("销量", num)
        .add_yaxis("价格", price)
        .set_global_opts(title_opts=opts.TitleOpts(title="Line-基本示例"))
    )
    make_snapshot(snapshot, c.render("../html/line.html"), "../png/line.png")


