# -*- coding: utf-8 -*-
import scrapy
import time
import random
from selenium.webdriver import ActionChains
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.common.exceptions import TimeoutException
from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver.chrome.options import Options
from analysis.tools import data_cleaning,register
from bs4 import BeautifulSoup
from analysis.items import AnalysisItem
from analysis.models import Good
import re


class TaobaoSpider(scrapy.Spider):
    name = 'taobao'
    # 爬虫允许的域名
    allowed_domains = ["taobao.com", "passport.alibaba.com",'csdn.net',"s.taobao.com",]
    # start_urls = ['https://login.taobao.com/']
    start_urls = ['https://login.taobao.com/member/login.jhtml']
    # 爬虫进行模拟登录的url
    login_url = 'https://login.taobao.com/member/login.jhtml'
    domain = "https://login.taobao.com/"
    username = "15155184302"
    password = "d827b3991h!!!"
    crawl_urls = 'https://buyertrade.taobao.com/trade/itemlist/list_bought_items.htm'
    base_url = ['https://s.taobao.com/search?q=']

    pages = 2
    i = 1

    headers = {
        'User-Agent': 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:35.0) Gecko/20100101 Firefox/35.0',
        'Content-Type': 'application/x-www-form-urlencoded',
        'Connection': 'Keep-Alive'
    }
    re_headers = {
        'user-agent': 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36',
        'referer': 'https://www.taobao.com/',
        'accept-encoding': 'gzip, deflate, b',
    }

    def start_requests(self):

        keys = self.settings.get('KEYS')
        self.browser, list = register()
        self.browser.get(self.base_url[0]+keys)
        self.browser.execute_script("window.scrollTo(0, document.body.scrollHeight)")
        url_i = self.browser.current_url
        html = self.browser.page_source
        yield scrapy.Request(
            url=self.base_url[0]+keys,
            headers=self.re_headers,
            cookies=list,
            callback=self.parse,
            meta={'html':html,'i':self.i,'url':url_i})
        # good = Good()
        # good.save()
        # goodres = Good.get(Person.name == 'Grandma L.')
        # driver = webdriver.PhantomJS()
        # driver.get("https://login.taobao.com/member/login.jhtml")
        # driver.find_element_by_id("TPL_username_1")
        # title_str =response.xpath("/html/head/title").extract()
        # self.post_login
        # print("已经下载到taobao的html2")
        # pass


    def parse(self, response):
        time.sleep(5)
        html = response.meta.get('html')
        i = response.meta.get("i")
        url_i = response.meta.get("url")
        i +=1
        if i > self.pages:
            return
        try:
            soup = BeautifulSoup(html,'html.parser')
            lists = soup.select('#mainsrp-itemlist > div > div > div > div')
            for list in lists:
                item = AnalysisItem()
                url = list.select('a[class="pic-link J_ClickStat J_ItemPicA"]')[0].attrs.get('href','')
                name = list.select("a[class='J_ClickStat']")[0].get_text().strip()
                name = data_cleaning(name)
                price = list.select('div[class="price g_price g_price-highlight"] strong')[0].get_text()

                # 提取购买人数
                purchase_num = list.select('div[class="deal-cnt"]')[0].get_text()
                if purchase_num.find("人付款") == -1:
                    continue
                num = re.search(r'\d+',purchase_num)
                num =int(num.group())

                # 提取单位
                unit = re.findall(r'(万)',purchase_num)
                unit = ''.join(unit)
                unit = (10000 if unit == '万' else 1)

                # 计算真实销量
                num = num * unit
                # 商品名称
                shop_name = list.select("a[class='shopname J_MouseEneterLeave J_ShopInfo']")[0].get_text().strip()
                shop_name = data_cleaning(shop_name)
                # 位置
                location = list.select("div[class='location']")[0].get_text().strip()
                # 提取省份
                province_name = location.split(' ')[0]
                location = data_cleaning(location)

                good = Good()
                good.url = url
                good.name = name
                good.price = price
                good.num = num
                good.shop_name = shop_name
                good.location = location
                good.province_name = province_name
                good.save()
                item['url'] = url
                item['name'] = name
                item['price'] = price
                item['num'] = num
                item['shop_name'] = shop_name
                # item['comment'] = comment
                item['location'] = location
                item['province_name'] = province_name
                yield item
            button = self.browser.find_elements(By.XPATH,'//a[@class="J_Ajax num icon-tag"]')[-1]
            button.click()
            time.sleep(random.random()*2)
            self.browser.execute_script("window.scrollTo(0, document.body.scrollHeight)")
            html = self.browser.page_source
            yield scrapy.Request(url=response.url,headers=self.re_headers,callback=self.parse,meta={'html':html,'i':i,'url':url_i},dont_filter=True)
        except Exception as e:
            time.sleep(10)
            print(e)
            self.browser.close()
            self.browser,list = register()
            self.browser.get(url=url_i)
            time.sleep(random.random()*2)
            self.browser.execute_script("window.scrollTo(0, document.body.scrollHeight)")
            html = self.browser.page_source
            yield scrapy.Request(url=response.url,headers=self.re_headers,callback=self.parse,meta={'html':html,'i':i,'url':url_i},dont_filter=True)

    def close(spider, reason):
        spider.browser.close()






