from pyecharts.charts import Map
from pyecharts import options as opts
from snapshot_pyppeteer import snapshot
from pyecharts.render import make_snapshot
from analysis.models import Good
from peewee import fn

# 计算销量
# province_num = df.groupby('province_name')['purchase_num'].sum().sort_values(ascending=False)
# 计算省top10
province_name = []
num = []
for good in Good.select(Good.province_name, fn.SUM(Good.num).alias('num')).group_by(Good.province_name).\
        order_by(fn.SUM(Good.num).desc()):
    province_name.append(good.province_name)
    num.append(good.num)

# 绘制地图
# map1 = Map(init_opts=opts.InitOpts(width='1350px', height='750px'))
map1 = Map()
map1.add("", [list(z) for z in zip(province_name, num)],
         maptype='china'
         )
map1.set_global_opts(title_opts=opts.TitleOpts(title='国内各省份口罩销量分布'),
                     visualmap_opts=opts.VisualMapOpts(max_=300000),
                     toolbox_opts=opts.ToolboxOpts()
                     )
make_snapshot(snapshot, map1.render("../html/province_map.html"), "../png/province_map.png")
