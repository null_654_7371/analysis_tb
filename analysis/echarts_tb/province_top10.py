# 导入包
from pyecharts.charts import Bar
from pyecharts import options as opts
from snapshot_pyppeteer import snapshot
from pyecharts.render import make_snapshot
from analysis.models import Good
from peewee import fn

# 计算省top10
province_name = []
num = []
for good in Good.select(Good.province_name, fn.SUM(Good.num).alias('num')).group_by(Good.province_name).\
        order_by(fn.SUM(Good.num).desc()).limit(10):
    province_name.append(good.province_name)
    num.append(good.num)


# 条形图
bar2 = Bar(init_opts=opts.InitOpts(width='1350px', height='750px'))
bar2.add_xaxis(province_name)
bar2.add_yaxis('sales_num', num)
bar2.set_global_opts(title_opts=opts.TitleOpts(title='口罩商品销量省份排名Top10'),
                     visualmap_opts=opts.VisualMapOpts(max_=max(num)))
make_snapshot(snapshot, bar2.render("../html/province_top10.html"), "../png/province_top10.png")