# 导入包
from pyecharts.charts import Bar
from pyecharts import options as opts
from snapshot_pyppeteer import snapshot
from pyecharts.render import make_snapshot
from analysis.models import Good
from peewee import fn

def tranform_price(x):
    if x <= 20:
        return '0~20'
    elif x <= 50:
        return '20~50'
    elif x <= 100:
        return '50~100'
    elif x <= 200:
        return '100~200'
    else:
        return '200~2500'


price = []
num = []
price_cut = []
for good in Good.select(Good.price, Good.num):
    price.append(good.price)
    num.append(good.num)

for x in price:
    price_cut.append(tranform_price(x))


dict = {}
for key in price_cut:
    dict[key] = dict.get(key, 0) + 1

bar3 = Bar()
bar3.add_xaxis(list(dict.keys()))
bar3.add_yaxis('price_num', list(dict.values()))
bar3.set_global_opts(title_opts=opts.TitleOpts(title='不同价格区间的商品数量'),
                     visualmap_opts=opts.VisualMapOpts(max_=1500))
make_snapshot(snapshot, bar3.render("../html/tranform_price.html"), "../png/tranform_price.png")