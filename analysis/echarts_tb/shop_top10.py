# 导入包
from pyecharts.charts import Bar
from pyecharts import options as opts
from snapshot_pyppeteer import snapshot
from pyecharts.render import make_snapshot
from analysis.models import Good
from peewee import fn

# 计算top10店铺
shop_name = []
num = []
for good in Good.select(Good.shop_name, fn.SUM(Good.num).alias('num')).group_by(Good.shop_name).\
        order_by(fn.SUM(Good.num).desc()).limit(10):
    shop_name.append(good.shop_name)
    num.append(good.num)

# 绘制柱形图
bar1 = Bar()
bar1.add_xaxis(shop_name)
bar1.add_yaxis('sales_num', num)
# bar1.set_series_opts(label_opts=opts.LabelOpts(position="right"))
bar1.set_global_opts(title_opts=opts.TitleOpts(title='口罩店铺商品销量Top10'),
                     xaxis_opts=opts.AxisOpts(axislabel_opts=opts.LabelOpts(rotate=-15)),
                     visualmap_opts=opts.VisualMapOpts(max_=max(num)))
make_snapshot(snapshot, bar1.render("../html/shop_top10.html"), "../png/shop_top10.png")
